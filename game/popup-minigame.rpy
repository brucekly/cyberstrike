init python:

    class PopupGameDisplayable(renpy.Displayable):

        def __init__(self, max_score):

            renpy.Displayable.__init__(self)

            self.POPUP_WIDTH = 417
            self.POPUP_HEIGHT = 243
            self.POPUP_GAP_X = 400
            self.POPUP_GAP_Y = 100
            self.POPUP_POSITION = 4
            self.MAX_SCORE = max_score

            self.score = 0

            self.popup = Image("popup.png", xsize=self.POPUP_WIDTH, ysize=self.POPUP_HEIGHT)

        def popup_pos(self, pos):
            """Return the location (in pixels) in one of the following
            screen areas:

            +---+---+---+    
            | 0 | 1 | 2 |
            +---+---+---+
            | 3 | 4 | 5 |
            +---+---+---+
            | 6 | 7 | 8 |
            +---+---+---+

            """
            
            # Draw a popup 
            return (
                (config.screen_width / 2) + (pos % 3 * self.POPUP_GAP_X) - self.POPUP_GAP_X,
                (config.screen_height / 2) + (pos / 3 * self.POPUP_GAP_Y) - self.POPUP_GAP_Y - 120,
            )

        def mouse_on_popup(self, mousex, mousey, pos):
            "Check whether the mouse is on top of the popup"

            popupx, popupy = self.popup_pos(pos)

            if mousex > popupx - self.POPUP_WIDTH / 2 and mousex < popupx + self.POPUP_WIDTH / 2:
                if mousey > popupy - self.POPUP_HEIGHT / 2 and mousey < popupy + self.POPUP_HEIGHT / 2:
                    return True

            return False

        def render(self, width, height, st, at):

            import random

            r = renpy.Render(width, height)

            def popup((px, py)):

                mi = renpy.render(self.popup, width, height, st, at)

                r.blit(mi, (int(px - self.POPUP_WIDTH / 2), int(py - self.POPUP_HEIGHT / 2)))

            popup(self.popup_pos(self.POPUP_POSITION))

            renpy.redraw(self, 0)

            return r

        def event(self, ev, x, y, st):

            import pygame
            import random

            # Check for a mouse press
            if ev.type == pygame.MOUSEBUTTONDOWN and ev.button == 1:

                # Check that a popup has been clicked
                if self.mouse_on_popup(x, y, self.POPUP_POSITION):
                    # Move the popup to a new location
                    self.POPUP_POSITION = random.randint(0, 8)

                    self.score += 1

                    # Check if the user has won the game
                    if self.score > self.MAX_SCORE:
                        return 0

                # Update the screen
                renpy.restart_interaction()                
                

screen popup_game(points):

    default popups = PopupGameDisplayable(points)

    add popups

    text _("Score: [popups.score]"):
        xpos 20
        xanchor 0
        ypos 20
        yanchor 0
        size 40

label play_popup_game:

    scene bg computer blank
    with fade
    
    # Hide the GUI
    window hide
    $ quick_menu = False

    call screen popup_game(points)

    # Show the GUI
    $ quick_menu = True
    window show

    jump after_game
    
